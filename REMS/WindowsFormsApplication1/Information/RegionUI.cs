﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using REMS;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;

namespace RemsUI.Information
{
    public partial class RegionUI : UserControl
    {
        private region _region;

        public RegionUI()
        {
            InitializeComponent();

            using (var context = new remsEntities())
            {
                List<region> regs = context.regions.ToList();
                _region = regs[0];
            }

            bindData();
        }


        public RegionUI(region reg)
            : this()
        {
            _region = reg;

            textBoxName.Text = _region.Name;
        }

        private void bindData()
        {

            Binding b = new Binding("Text", _region, "Name");

            b.ControlUpdateMode = ControlUpdateMode.OnPropertyChanged;
            b.DataSourceUpdateMode = DataSourceUpdateMode.OnPropertyChanged;

            textBoxName.DataBindings.Add(b);
        }

        private void button1_Click(object sender, EventArgs e)
        {

            using (var ctx = new remsEntities())
            {
                ctx.regions.Attach(_region);
                ctx.Entry(_region).State = EntityState.Modified;
                ctx.SaveChanges();
            }
            //_region.
        }
    }
}
