﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RemsUI.Information;

namespace RemsUI
{
    public partial class InformationForm : Form
    {
        public InformationForm()
        {
            InitializeComponent();

            windowUI.panelUI.Controls.Add(new RegionUI());
        }
    }
}
