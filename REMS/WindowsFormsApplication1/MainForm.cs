﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using REMS;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;

namespace RemsUI
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
            using (var context = new remsEntities())
            {
                List<region> regs = context.regions.ToList();
                System.Diagnostics.Debug.WriteLine(regs[0].Name);
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.IsMdiContainer = true;
        }

        private void button1_Click(object sender, EventArgs e)
        {
           

        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            InformationForm IF = new InformationForm();

            IF.MdiParent = this;

            IF.Show();
        }
    }
}
