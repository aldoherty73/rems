//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace REMS
{
    using System;
    using System.Collections.Generic;
    
    public partial class crop
    {
        public crop()
        {
            this.experiments = new HashSet<experiment>();
        }
    
        public int CropID { get; set; }
        public string Name { get; set; }
        public string Notes { get; set; }
    
        public virtual ICollection<experiment> experiments { get; set; }
    }
}
