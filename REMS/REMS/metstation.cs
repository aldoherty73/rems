//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace REMS
{
    using System;
    using System.Collections.Generic;
    
    public partial class metstation
    {
        public metstation()
        {
            this.experiments = new HashSet<experiment>();
            this.metdatas = new HashSet<metdata>();
            this.metinfoes = new HashSet<metinfo>();
        }
    
        public int MetStationID { get; set; }
        public string Name { get; set; }
        public Nullable<double> Latitude { get; set; }
        public Nullable<double> Longitude { get; set; }
        public Nullable<double> Elevation { get; set; }
        public Nullable<double> amp { get; set; }
        public Nullable<double> tav { get; set; }
        public string Notes { get; set; }
    
        public virtual ICollection<experiment> experiments { get; set; }
        public virtual ICollection<metdata> metdatas { get; set; }
        public virtual ICollection<metinfo> metinfoes { get; set; }
    }
}
