//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace REMS
{
    using System;
    using System.Collections.Generic;
    
    public partial class experiment
    {
        public experiment()
        {
            this.expinfoes = new HashSet<expinfo>();
            this.researcherlists = new HashSet<researcherlist>();
            this.treatments = new HashSet<treatment>();
        }
    
        public int ExpID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public Nullable<int> CropID { get; set; }
        public Nullable<int> FieldID { get; set; }
        public Nullable<System.DateTime> BeginDate { get; set; }
        public Nullable<System.DateTime> EndDate { get; set; }
        public Nullable<int> MetStationID { get; set; }
        public string ExpDesign { get; set; }
        public Nullable<short> Reps { get; set; }
        public Nullable<int> Rating { get; set; }
        public string Notes { get; set; }
        public Nullable<int> PlantingMethodID { get; set; }
        public string PlantingNotes { get; set; }
    
        public virtual crop crop { get; set; }
        public virtual ICollection<expinfo> expinfoes { get; set; }
        public virtual ICollection<researcherlist> researcherlists { get; set; }
        public virtual ICollection<treatment> treatments { get; set; }
        public virtual field field { get; set; }
        public virtual method method { get; set; }
        public virtual metstation metstation { get; set; }
    }
}
