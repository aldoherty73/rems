//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace REMS
{
    using System;
    using System.Collections.Generic;
    
    public partial class plot
    {
        public plot()
        {
            this.plotdatas = new HashSet<plotdata>();
            this.soildatas = new HashSet<soildata>();
            this.soillayerdatas = new HashSet<soillayerdata>();
        }
    
        public int PlotID { get; set; }
        public Nullable<int> TreatmentID { get; set; }
        public Nullable<int> Rep { get; set; }
        public Nullable<int> Col { get; set; }
        public Nullable<int> Row { get; set; }
    
        public virtual ICollection<plotdata> plotdatas { get; set; }
        public virtual ICollection<soildata> soildatas { get; set; }
        public virtual ICollection<soillayerdata> soillayerdatas { get; set; }
        public virtual treatment treatment { get; set; }
    }
}
