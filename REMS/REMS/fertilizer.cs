//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace REMS
{
    using System;
    using System.Collections.Generic;
    
    public partial class fertilizer
    {
        public fertilizer()
        {
            this.fertilizations = new HashSet<fertilization>();
        }
    
        public int FertilizerID { get; set; }
        public string Name { get; set; }
        public Nullable<double> N_ { get; set; }
        public Nullable<double> P_ { get; set; }
        public Nullable<double> K_ { get; set; }
        public Nullable<double> Ca_ { get; set; }
        public Nullable<double> S_ { get; set; }
        public Nullable<double> Other_ { get; set; }
        public string Other { get; set; }
        public string Notes { get; set; }
    
        public virtual ICollection<fertilization> fertilizations { get; set; }
    }
}
